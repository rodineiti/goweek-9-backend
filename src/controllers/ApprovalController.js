const Booking = require('../models/Booking');
const User = require('../models/User');

module.exports = {
    async approval(request, response) {
        const { booking_id } = request.params;
        
        let booking = await Booking.findById(booking_id).populate('spot');
        booking.approved = true;

        const bookingUserSocket = request.connectedUsers[booking.user];

        if (bookingUserSocket) {
            request.io.to(bookingUserSocket).emit('booking_response', booking);
        }

        await booking.save();

        return response.json(booking);
    },
    async reject(request, response) {
        const { booking_id } = request.params;
        
        let booking = await Booking.findById(booking_id).populate('spot');
        booking.approved = false;

        const bookingUserSocket = request.connectedUsers[booking.user];

        if (bookingUserSocket) {
            request.io.to(bookingUserSocket).emit('booking_response', booking);
        }

        await booking.save();

        return response.json(booking);
    },
}