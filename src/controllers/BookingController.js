const Booking = require('../models/Booking');
const User = require('../models/User');
const Spot = require('../models/Spot');

module.exports = {
    async store(request, response) {
        const { user_id } = request.headers;
        const { spot_id } = request.params;
        const { date } = request.body;

        //5d94b62e439cb91590edd9a0
        let user = await User.findById(user_id);

        if (!user) {
            return response.json({error: 'User does not exists'});
        }

        let spot = await Spot.findById(spot_id);

        if (!spot) {
            return response.json({error: 'Spot does not exists'});
        }

        const booking = await Booking.create({ user: user_id, spot: spot_id, date });

        await booking.populate('spot').populate('user').execPopulate();

        const ownerSocket = request.connectedUsers[booking.spot.user];

        if (ownerSocket) {
            request.io.to(ownerSocket).emit('booking_request', booking);
        }

        return response.json(booking);
    },
}