const Spot = require('../models/Spot');
const User = require('../models/User');

module.exports = {
    async show(request, response) {
        const { user_id } = request.headers;

        //5d94b62e439cb91590edd9a0
        let user = await User.findById(user_id);

        if (!user) {
            return response.json({error: 'User does not exists'});
        }

        const spots = await Spot.find({ user: user_id });

        return response.json(spots);
    },
}